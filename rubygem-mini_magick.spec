%global gem_name mini_magick
Name:                rubygem-%{gem_name}
Version:             5.0.1
Release:             1
Summary:             Manipulate images with minimal use of memory via ImageMagick
License:             MIT
URL:                 https://github.com/minimagick/minimagick
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1:             https://github.com/minimagick/minimagick/archive/v%{version}.tar.gz
Requires:            ImageMagick
BuildRequires:       ruby(release) rubygems-devel ruby rubygem(rspec) rubygem(webmock) ImageMagick rubygem(rexml) rubygem-bundler rubygem-rspec-core
BuildArch:           noarch
%description
A ruby wrapper for ImageMagick command line. Using MiniMagick the ruby
processes memory remains small (it spawns ImageMagick's command line program
mogrify which takes up some memory as well, but is much smaller compared
to RMagick).

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
tar xzvf %{SOURCE1}
cd  minimagick-%{version}
sed -i -e '/require "bundler/ s/^/#/g' \
  spec/spec_helper.rb
sed -i '/^    it "identifies when gm exists" do$/,/    end/ s/^/#/g' \
  spec/lib/mini_magick/utilities_spec.rb

bundle exec rspec
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%doc %{gem_instdir}/README.md
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Rakefile

%changelog
* Thu Jul 25 2024 Funda Wang <fundawang@yeah.net> - 5.0.1-1
- Update to 5.0.1

* Fri Nov 17 2023 liyanan <liyanan32@huawei.com> - 4.12.0-1
- Update to 4.12.0

* Wed Aug 02 2023 chenchen <chen_aka_jan@163.com> - 4.8.0-5
- Replacei deprecated File.exists with File.exist due to ruby upgrade to 3.2.2

* Tue Mar 29 2022 liyanan <liyanan32@huawei.com> - 1.0.2-4
- Fix no implicit conversion of Hash into Integer

* Tue Apr 13 2021 wangxiao65 <wangxiao65@huawei.com> - 1.0.2-3
- Fix CVE-2019-13574

* Tue Sep 8 2020 yanan li <liyanan032@huawei.com> - 1.0.2-2
- fix build fail

* Wed Aug 19 2020 geyanan <geyanan2@huawei.com> - 4.8.0-1
- package init
